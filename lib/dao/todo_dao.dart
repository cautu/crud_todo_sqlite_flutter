import 'package:crud_todo_app/database/database.dart';
import 'package:crud_todo_app/model/todo.dart';

class TodoDao {
  final dbProvider = DatabaseProvider.dbProvider;

  Future<int> createTodo(Todo todo) async {
    final db = await dbProvider.database;
    final result = db.insert(table, todo.toDatabaseJson());
    return result;
  }

  Future<List<Todo>> getTodos({List<String> columns, String query}) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> result;

    if (query != null) {
      if (query.isNotEmpty) {
        result = await db.query(table,
            columns: columns,
            where: 'description LIKE ?',
            whereArgs: ["%$query%"]);
      }
    } else {
      result = await db.query(table, columns: columns);
    }
    List<Todo> todos = result.isNotEmpty
        ? result.map((item) => Todo.fromDatabaseJson(item)).toList()
        : [];
    return todos;
  }

  Future<int> updateTodo(Todo todo) async {
    final db = await dbProvider.database;
    final result = await db.update(table, todo.toDatabaseJson(),
        where: "id = ? ", whereArgs: [todo.id]);
    return result;
  }

  Future<int> deleteTodo(int id) async {
    final db = await dbProvider.database;
    final result = db.delete(table, where: 'id = ?', whereArgs: [id]);
    return result;
  }

  Future deleteAllTodos() async {
    final db = await dbProvider.database;
    final result = await db.delete(table);
    return result;
  }
}
