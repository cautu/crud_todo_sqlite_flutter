import 'dart:async';

import 'package:crud_todo_app/model/todo.dart';
import 'package:crud_todo_app/repository/todo_respository.dart';

class TodoBloc {
  final _todoRespository = TodoRespository();

  final _todoController = StreamController<List<Todo>>();
  // final _todoController = StreamController<List<Todo>>.broadcast();

  get todos => _todoController.stream;

  TodoBloc() {
    getTodos();
  }

  getTodos({String query}) async {
    _todoController.sink.add(await _todoRespository.getAllTodos(query: query));
  }

  addTodo(Todo todo) async {
    await _todoRespository.insertTodo(todo);
    getTodos();
  }

  updateTodo(Todo todo) async {
    await _todoRespository.updateTodo(todo);
    getTodos();
  }

  deleteTodoById(int id) async {
    _todoRespository.deleteTodoById(id);
    getTodos();
  }

  dispose() {
    _todoController.close();
  }
}
